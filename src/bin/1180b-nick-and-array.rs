use std::io::{self, BufRead};

fn compute(mut input: Vec<i64>) -> Vec<i64> {
    for item in input.iter_mut() {
        if *item >= 0 {
            *item = -*item - 1;
        }
    }

    // All items are negative now. If we have an even count, we're done.
    if input.len() % 2 == 0 {
        return input
    }

    // Otherwise, we take the highest value, and swap it back to positive.
    let (min_idx, _min_item) = input.iter().enumerate()
        .min_by_key(|a| a.1).unwrap_or((0, &-1));
    input[min_idx] = -input[min_idx] - 1;

    input
}

fn main() {
    let stdin = io::stdin();
    let line = stdin.lock().lines().next().expect("input").expect("UTF-8");
    let n: usize = line.parse().expect("Numeric value");

    let line = stdin.lock().lines().next().expect("input").expect("UTF-8");
    let a: Vec<_> = line.split(' ').map(|sub| sub.parse().expect("Numbers plx")).collect();

    assert_eq!(n, a.len());

    let res = compute(a);

    for item in res {
        print!("{} ", item);
    }
}

#[test]
fn nick_vectors() {
    let in_out = [
        (vec![2, 2, 2, 2], vec![-3, -3, -3, -3]),
        (vec![0], vec![0]),
        (vec![], vec![]),
        (vec![0, 0], vec![-1, -1]),
        (vec![1, 1], vec![-2, -2]),
        (vec![-3, -3, 2], vec![-3, -3, 2]),
        (vec![1, 0, 1], vec![-2, -1, 1]),
    ];

    for (idx, (i, o)) in in_out.iter().enumerate() {
        let test = compute(i.clone());
        assert_eq!(test, *o,
            "{}th should be {:?}, but got {:?}", idx, *o, test);
    }
}

