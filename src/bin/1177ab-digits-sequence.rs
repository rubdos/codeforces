use std::io::{self, BufRead};
use std::collections::VecDeque;

struct DigitSequenceIterator {
    inner: u64,
    state: VecDeque<char>,
}

impl Iterator for DigitSequenceIterator {
    type Item = char;

    fn next(&mut self) -> Option<char> {
        if self.state.len() == 0 {
            self.state.extend(self.inner.to_string().chars());;

            self.inner += 1;
        }

        self.state.pop_front()
    }
}

impl DigitSequenceIterator {
    #[allow(dead_code)]
    pub fn new() -> Self {
        DigitSequenceIterator {
            inner: 0,
            state: VecDeque::new(),
        }
    }

    pub fn skip(mut to_skip: u64) -> impl Iterator<Item=char> {
        let mut inner = 0;

        if to_skip >= 10 {
            inner = 10;
            to_skip -= 10;
        }

        let mut can_jump = 90;
        let mut i = 2;
        while to_skip > can_jump * i {
            to_skip -= can_jump * i;
            inner *= 10;

            can_jump *= 10;
            i += 1;
        }

        // Now, all numbers between inner and the end are `i` wide.
        // So, to_skip/i is the amount of numbers we can still skip.
        if i > 2 {
            inner += to_skip/i;
            to_skip = to_skip % i;
        }

        DigitSequenceIterator {
            inner,
            state: VecDeque::new(),
        }.skip(to_skip as usize)
    }
}

fn main() {
    let stdin = io::stdin();
    let line = stdin.lock().lines().next().expect("input").expect("UTF-8");
    let index: u64 = line.parse().expect("Numeric value");

    println!("{}", DigitSequenceIterator::skip(index).next().unwrap());
}

#[test]
fn vectors() {
    let in_out = [(7, '7'), (21, '5'), (188, '9'), (192, '0'), (417, '5')];

    for (i, o) in &in_out {
        let test = DigitSequenceIterator::skip(*i).next().unwrap();
        assert_eq!(test, *o,
            "{}th should be {}, but got {}", i, *o, test);
    }
}
