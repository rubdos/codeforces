use std::io::{self, BufRead};

use std::collections::VecDeque;

fn compute(mut input: VecDeque<u64>, m: u64) -> (u64, u64) {
    for _op in 1..m {
        let a = input.pop_front().unwrap();
        let b = input.pop_front().unwrap();
        if a > b {
            input.push_front(a);
            input.push_back(b);
        } else {
            input.push_front(b);
            input.push_back(a);
        }
    }

    (input.pop_front().unwrap(), input.pop_front().unwrap())
}

fn main() {
    let stdin = io::stdin();
    let line = stdin.lock().lines().next().expect("input").expect("UTF-8");
    let mut split = line.split(' ');
    let n: u64 = split.next().unwrap().parse().expect("numeric");
    let q: u64 = split.next().unwrap().parse().expect("numeric");

    let line = stdin.lock().lines().next().expect("input").expect("UTF-8");
    let a: VecDeque<_> = line.split(' ').map(|sub| sub.parse().expect("Numbers plx")).collect();

    for line in stdin.lock().lines() {
        let line = line.expect("UTF-8");
        let m = line.parse().expect("Nums plx");

        let (a, b) = compute(a.clone(), m);
        println!("{} {}", a, b);
    }
}

#[test]
fn vectors() {
    let in_out = [
        ((vec![1, 2, 3, 4, 5], 1), (1, 2)),
        ((vec![1, 2, 3, 4, 5], 2), (2, 3)),
        ((vec![1, 2, 3, 4, 5], 10), (5, 2)),
    ];

    for (idx, ((deq, q), o)) in in_out.iter().enumerate() {
        let test = compute(VecDeque::from(deq.clone()), *q);
        assert_eq!(test, *o,
            "{}th should be {:?}, but got {:?}", idx, *o, test);
    }
}

