use std::io::{self, BufRead};

fn compute(order: u64) -> u64 {
    let order = order as i64;

    // order * (a1 + an) / 2
    let an = order * 2 - 1;
    let an1 = (order-1) * 2 - 1;
    let a1 = 1;

    (order * (a1+an) / 2 + (order - 1) * (a1+an1)/2) as u64
}

fn main() {
    let stdin = io::stdin();
    let line = stdin.lock().lines().next().expect("input").expect("UTF-8");
    let order: u64 = line.parse().expect("Numeric value");

    let sum = compute(order);

    println!("{}", sum);
}

#[test]
fn vectors() {
    let in_out = [(1, 1), (2, 5), (3, 13)];

    for (i, o) in &in_out {
        let test = compute(*i);
        assert_eq!(test, *o,
            "{}th should be {}, but got {}", i, *o, test);
    }
}
