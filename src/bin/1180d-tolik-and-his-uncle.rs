use std::io::{self, BufRead};

struct Field {
    visited: Vec<(i32, i32)>,
    to_visit: Vec<(i32, i32)>,
    used_jumps: Vec<(i32, i32)>,
    current: (i32, i32),
}

impl Field {
    fn new(n: i32, m: i32) -> Self {
        let mut visited = Vec::with_capacity((n * m) as usize);
        let mut to_visit = Vec::with_capacity((n * m) as usize);

        visited.push((0, 0));

        for i in 0..n {
            for j in 0..m {
                if i == 0 && j == 0 {
                    continue
                }
                to_visit.push((i, j));
            }
        }

        Field {
            visited,
            to_visit,
            used_jumps: Vec::with_capacity((n*m) as usize),
            current: (0, 0),
        }
    }

    fn jumps(self) -> impl Iterator<Item=Field> {
        let Field {
            visited,
            to_visit,
            used_jumps,
            current,
        } = self;

        let to_visit2 = to_visit.clone();
        let used_jumps2 = used_jumps.clone();
        let used_jumps3 = used_jumps.clone();

        to_visit.into_iter().map(move |(x, y)| {
            let (dx, dy) = (x - current.0, y - current.1);
            ((x, y), (dx, dy))
        })
        .filter(move |(_pos, jump)| !used_jumps2.contains(jump))
        .map(move |(current, jump)| {
            let mut visited = visited.clone();
            visited.push(current);
            let mut used_jumps = used_jumps3.clone();
            used_jumps.push(jump);

            let to_visit = to_visit2.iter().cloned().filter(|v| *v != current).collect();

            Field {
                visited,
                to_visit,
                used_jumps,
                current,
            }
        })
    }

    fn is_solved(&self) -> bool {
        self.to_visit.len() == 0
    }
}

fn compute((n, m): (i32, i32)) -> Result<Vec<(i32, i32)>, ()> {
    let field = Field::new(n, m);

    if field.is_solved() {
        return Ok(field.visited);
    }

    let mut stack = Vec::new();
    stack.push(field.jumps());

    while let Some(jumps) = stack.pop() {
        for jump in jumps {
            if jump.is_solved() {
                assert_eq!(jump.visited.len() as i32, n*m);
                return Ok(jump.visited);
            }

            stack.push(jump.jumps());
        }
    }

    Err(())
}

fn main() {
    let stdin = io::stdin();
    let line = stdin.lock().lines().next().expect("input").expect("UTF-8");
    let mut split = line.split(' ');
    let n: i32 = split.next().unwrap().parse().expect("numeric");
    let m: i32 = split.next().unwrap().parse().expect("numeric");

    let res = compute((n, m));
    match res {
        Ok(res) => for (a, b) in res {
            println!("{} {}", a + 1, b + 1);
        },
        Err(()) => println!("-1"),
    }
}

#[test]
fn vectors() {
    let in_out = [
        ((1, 1), Ok(vec![(0, 0)])),
        ((2, 3), Ok(vec![(0, 0), (0, 2), (0, 1), (1, 1), (1, 2), (1, 0)])),
    ];

    for (idx, (i, o)) in in_out.iter().enumerate() {
        let test = compute(i.clone());
        assert_eq!(test, *o,
            "{}th should be {:?}, but got {:?}", idx, *o, test);
    }
}


